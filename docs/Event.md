

# Event


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **Integer** |  |  [optional]
**blockId** | **String** |  |  [optional]
**blockNumber** | **Integer** |  |  [optional]
**date** | **Integer** |  |  [optional]
**decimals** | **Integer** |  |  [optional]
**denomination** | **String** |  |  [optional]
**destination** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**meta** | **Object** |  |  [optional]
**source** | **String** |  |  [optional]
**transactionId** | **String** |  |  [optional]
**type** | **String** |  |  [optional]



