

# FeeEstimateEstimatedFees

Object containing fast, medium, slow fees

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fast** | **Object** |  |  [optional]
**medium** | **Object** |  |  [optional]
**slow** | **Object** |  |  [optional]



