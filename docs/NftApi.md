# NftApi

All URIs are relative to *https://ubiquity.api.blockdaemon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**explorerGetCollection**](NftApi.md#explorerGetCollection) | **GET** /v1/nft/{protocol}/{network}/collection/{id} | 
[**explorerListAssets**](NftApi.md#explorerListAssets) | **GET** /v1/nft/{protocol}/{network}/assets | 
[**explorerListCollections**](NftApi.md#explorerListCollections) | **GET** /v1/nft/{protocol}/{network}/collections | 
[**explorerListEvents**](NftApi.md#explorerListEvents) | **GET** /v1/nft/{protocol}/{network}/events | 



## explorerGetCollection

> GetCollectionResponse explorerGetCollection(protocol, network, id)



### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.NftApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        NftApi apiInstance = new NftApi(defaultClient);
        String protocol = "protocol_example"; // String | Coin platform handle
        String network = "network_example"; // String | Which network to target
        String id = "id_example"; // String | Mapped to URL query parameter 'uuid'
        try {
            GetCollectionResponse result = apiInstance.explorerGetCollection(protocol, network, id);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling NftApi#explorerGetCollection");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **String**| Coin platform handle |
 **network** | **String**| Which network to target |
 **id** | **String**| Mapped to URL query parameter &#39;uuid&#39; |

### Return type

[**GetCollectionResponse**](GetCollectionResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |


## explorerListAssets

> ListAssetsResponse explorerListAssets(protocol, network, walletAddress, contractAddress, tokenIdValue, collectionName, sortBy, order, pageSize, pageToken, attributes)



### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.NftApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        NftApi apiInstance = new NftApi(defaultClient);
        String protocol = "protocol_example"; // String | Coin platform handle
        String network = "network_example"; // String | Which network to target
        String walletAddress = "walletAddress_example"; // String | Mapped to URL query parameter `wallet_address`
        String contractAddress = "contractAddress_example"; // String | Mapped to URL query parameter `contract_address`
        Long tokenIdValue = 56L; // Long | The int64 value.
        String collectionName = "collectionName_example"; // String | Mapped to URL query parameter `collection_name`
        String sortBy = "sortBy_example"; // String | One of: name, token_id, mint_date
        String order = "order_example"; // String | Mapped to URL query parameter `order` One of: asc, desc
        Integer pageSize = 56; // Integer | Mapped to URL query parameter `page_size`
        String pageToken = "pageToken_example"; // String | Mapped to URL query parameter `page_token` base64 encoded cursor
        List<String> attributes = Arrays.asList(); // List<String> | Mapped to URL query parameter `attributes`
        try {
            ListAssetsResponse result = apiInstance.explorerListAssets(protocol, network, walletAddress, contractAddress, tokenIdValue, collectionName, sortBy, order, pageSize, pageToken, attributes);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling NftApi#explorerListAssets");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **String**| Coin platform handle |
 **network** | **String**| Which network to target |
 **walletAddress** | **String**| Mapped to URL query parameter &#x60;wallet_address&#x60; | [optional]
 **contractAddress** | **String**| Mapped to URL query parameter &#x60;contract_address&#x60; | [optional]
 **tokenIdValue** | **Long**| The int64 value. | [optional]
 **collectionName** | **String**| Mapped to URL query parameter &#x60;collection_name&#x60; | [optional]
 **sortBy** | **String**| One of: name, token_id, mint_date | [optional]
 **order** | **String**| Mapped to URL query parameter &#x60;order&#x60; One of: asc, desc | [optional]
 **pageSize** | **Integer**| Mapped to URL query parameter &#x60;page_size&#x60; | [optional]
 **pageToken** | **String**| Mapped to URL query parameter &#x60;page_token&#x60; base64 encoded cursor | [optional]
 **attributes** | **List&lt;String&gt;**| Mapped to URL query parameter &#x60;attributes&#x60; | [optional]

### Return type

[**ListAssetsResponse**](ListAssetsResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |


## explorerListCollections

> ListCollectionResponse explorerListCollections(protocol, network, contractAddress, collectionName, sortBy, order, pageSize, pageToken)



### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.NftApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        NftApi apiInstance = new NftApi(defaultClient);
        String protocol = "protocol_example"; // String | Coin platform handle
        String network = "network_example"; // String | Which network to target
        List<String> contractAddress = Arrays.asList(); // List<String> | Mapped to URL query parameter 'contract_address'
        List<String> collectionName = Arrays.asList(); // List<String> | Mapped to URL query parameter 'collection_name'
        String sortBy = "sortBy_example"; // String | Sort by one of: name
        String order = "order_example"; // String | Mapped to URL query parameter `order` One of: asc, desc
        Integer pageSize = 56; // Integer | Mapped to URL query parameter `page_size`
        String pageToken = "pageToken_example"; // String | Mapped to URL query parameter `page_token` base64 encoded cursor
        try {
            ListCollectionResponse result = apiInstance.explorerListCollections(protocol, network, contractAddress, collectionName, sortBy, order, pageSize, pageToken);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling NftApi#explorerListCollections");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **String**| Coin platform handle |
 **network** | **String**| Which network to target |
 **contractAddress** | **List&lt;String&gt;**| Mapped to URL query parameter &#39;contract_address&#39; | [optional]
 **collectionName** | **List&lt;String&gt;**| Mapped to URL query parameter &#39;collection_name&#39; | [optional]
 **sortBy** | **String**| Sort by one of: name | [optional]
 **order** | **String**| Mapped to URL query parameter &#x60;order&#x60; One of: asc, desc | [optional]
 **pageSize** | **Integer**| Mapped to URL query parameter &#x60;page_size&#x60; | [optional]
 **pageToken** | **String**| Mapped to URL query parameter &#x60;page_token&#x60; base64 encoded cursor | [optional]

### Return type

[**ListCollectionResponse**](ListCollectionResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |


## explorerListEvents

> ListEventResponse explorerListEvents(protocol, network, contractAddress, walletAddress, tokenId, eventType, sortBy, order, pageSize, pageToken)



### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.NftApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        NftApi apiInstance = new NftApi(defaultClient);
        String protocol = "protocol_example"; // String | Coin platform handle
        String network = "network_example"; // String | Which network to target
        String contractAddress = "contractAddress_example"; // String | mapped to URL query parameter 'contract_address'
        String walletAddress = "walletAddress_example"; // String | mapped to URL query parameter 'wallet_address'
        Long tokenId = 56L; // Long | mapped to URL query parameter 'token_id'
        String eventType = "eventType_example"; // String | mapped to URL query parameter 'event_type'
        String sortBy = "sortBy_example"; // String | Sort by one of: timestamp
        String order = "order_example"; // String | Mapped to URL query parameter `order` One of: asc, desc
        Integer pageSize = 56; // Integer | Mapped to URL query parameter `page_size`
        String pageToken = "pageToken_example"; // String | Mapped to URL query parameter `page_token` base64 encoded cursor
        try {
            ListEventResponse result = apiInstance.explorerListEvents(protocol, network, contractAddress, walletAddress, tokenId, eventType, sortBy, order, pageSize, pageToken);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling NftApi#explorerListEvents");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **String**| Coin platform handle |
 **network** | **String**| Which network to target |
 **contractAddress** | **String**| mapped to URL query parameter &#39;contract_address&#39; | [optional]
 **walletAddress** | **String**| mapped to URL query parameter &#39;wallet_address&#39; | [optional]
 **tokenId** | **Long**| mapped to URL query parameter &#39;token_id&#39; | [optional]
 **eventType** | **String**| mapped to URL query parameter &#39;event_type&#39; | [optional]
 **sortBy** | **String**| Sort by one of: timestamp | [optional]
 **order** | **String**| Mapped to URL query parameter &#x60;order&#x60; One of: asc, desc | [optional]
 **pageSize** | **Integer**| Mapped to URL query parameter &#x60;page_size&#x60; | [optional]
 **pageToken** | **String**| Mapped to URL query parameter &#x60;page_token&#x60; base64 encoded cursor | [optional]

### Return type

[**ListEventResponse**](ListEventResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

