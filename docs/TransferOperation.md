

# TransferOperation


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  | 
**event** | **String** |  |  [optional]
**detail** | [**Transfer**](Transfer.md) |  | 



