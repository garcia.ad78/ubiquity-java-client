

# ListCollectionResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**List&lt;Collection&gt;**](Collection.md) |  |  [optional]
**meta** | [**Meta**](Meta.md) |  |  [optional]



