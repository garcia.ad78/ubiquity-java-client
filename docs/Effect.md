

# Effect

Effects are the state changes on an account

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**balanceChanges** | [**Map&lt;String, BalanceChange&gt;**](BalanceChange.md) | Balance changes by asset |  [optional]



