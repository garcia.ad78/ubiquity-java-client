

# TxMinify

Transaction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Unique transaction identifier |  [optional]
**date** | **Long** | Unix timestamp |  [optional]
**blockId** | **String** | ID of block. |  [optional]
**blockNumber** | **Long** | Height of block, |  [optional]
**confirmations** | **Long** | Total transaction confirmations |  [optional]



