

# BlockIdentifier


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **Long** | Block number |  [optional]
**id** | **String** | Block hash |  [optional]
**parentId** | **String** | Parent block hash |  [optional]



