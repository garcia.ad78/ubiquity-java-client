/*
 * Ubiquity REST API
 * Ubiquity provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ### Protocols #### Mainnet The following protocols are currently supported: * bitcoin * ethereum * polkadot * xrp * algorand * stellar * dogecoin * oasis * near * litecoin * bitcoincash * tezos  #### Testnet * bitcoin/testnet * ethereum/ropsten * dogecoin/testnet * litecoin/testnet * bitcoincash/testnet  #### Native Ubiquity provides native access to all Blockchain nodes it supports. * bitcoin/(mainnet | testnet) - [RPC Documentation](https://developer.bitcoin.org/reference/rpc/) * ethereum/(mainnet | ropsten) - [RPC Documentation](https://ethereum.org/en/developers/docs/apis/json-rpc/) * polkadot/mainnet - [Sidecar API Documentation](https://paritytech.github.io/substrate-api-sidecar/dist/) * polkadot/mainnet/http-rpc - [Polkadot RPC Documentation](https://polkadot.js.org/docs/substrate/rpc/) * algorand/mainnet - [Algod API Documentation](https://developer.algorand.org/docs/reference/rest-apis/algod/) * stellar/mainnet - [Stellar Horizon API Documentation](https://developers.stellar.org/api) * dogecoin/(mainnet | testnet) - [Dogecoin API Documentaion](https://developer.bitcoin.org/reference/rpc/) * oasis/mainnet - [Oasis Rosetta Gateway Documentation](https://www.rosetta-api.org/docs/api_identifiers.html#network-identifier) * near/mainnet - [NEAR RPC Documentation](https://docs.near.org/docs/api/rpc) * litecoin/mainnet - [Litecoin RPC Documentation](https://litecoin.info/index.php/Litecoin_API) * bitcoincash/mainnet - [Bitcoin Cash RPC Documentation](https://docs.bitcoincashnode.org/doc/json-rpc/) * tezos/mainnet - [Tezos RPC Documentation](https://tezos.gitlab.io/developer/rpc.html)   A full URL example: https://ubiquity.api.blockdaemon.com/v1/bitcoin/mainnet  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available. 
 *
 * The version of the OpenAPI document: 3.0.0
 * Contact: support@blockdaemon.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.gitlab.blockdaemon.ubiquity.api;

import com.gitlab.blockdaemon.ubiquity.invoker.*;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.model.Error;
import com.gitlab.blockdaemon.ubiquity.model.FeeEstimate;
import com.gitlab.blockdaemon.ubiquity.model.SignedTx;
import com.gitlab.blockdaemon.ubiquity.model.Tx;
import com.gitlab.blockdaemon.ubiquity.model.TxConfirmation;
import com.gitlab.blockdaemon.ubiquity.model.TxOutput;
import com.gitlab.blockdaemon.ubiquity.model.TxPage;
import com.gitlab.blockdaemon.ubiquity.model.TxReceipt;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for TransactionsApi
 */
public class TransactionsApiTest {

    private final TransactionsApi api = new TransactionsApi();

    /**
     * Get fee estimate
     *
     * Get a fee estimation in decimals from the ubiquity fee estimation service. Currently supported for Bitcoin and Ethereum. Endpoint will return 3 fee estimations fast, medium and slow 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void feeEstimateTest() throws ApiException {
        //String platform = null;
        //String network = null;
        //FeeEstimate response = api.feeEstimate(platform, network);
        // TODO: test validations
    }

    /**
     * Transaction By Hash
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getTxTest() throws ApiException {
        //String platform = null;
        //String network = null;
        //String id = null;
        //Tx response = api.getTx(platform, network, id);
        // TODO: test validations
    }

    /**
     * Transaction output by hash and index
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getTxByHashAndIndexTest() throws ApiException {
        //String platform = null;
        //String network = null;
        //String id = null;
        //Integer index = null;
        //TxOutput response = api.getTxByHashAndIndex(platform, network, id, index);
        // TODO: test validations
    }

    /**
     * Transaction confirmations By Hash
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getTxConfirmationsTest() throws ApiException {
        //String platform = null;
        //String network = null;
        //String id = null;
        //TxConfirmation response = api.getTxConfirmations(platform, network, id);
        // TODO: test validations
    }

    /**
     * Latest transactions of a protocol
     *
     * Gets transactions from oldest to newest. This call uses pagination. 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getTxsTest() throws ApiException {
        //String platform = null;
        //String network = null;
        //String order = null;
        //String continuation = null;
        //Integer limit = null;
        //String assets = null;
        //TxPage response = api.getTxs(platform, network, order, continuation, limit, assets);
        // TODO: test validations
    }

    /**
     * Submit a signed transaction
     *
     * Submit a signed transaction to the network.  **Note**: A successful transaction may still be rejected on chain or not processed due to a too low fee. You can monitor successful transactions through Ubiquity websockets. 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void txSendTest() throws ApiException {
        //String platform = null;
        //String network = null;
        //SignedTx signedTx = null;
        //TxReceipt response = api.txSend(platform, network, signedTx);
        // TODO: test validations
    }

}
