package com.gitlab.blockdaemon.ubiquity.model;

public enum Order {
	DESC("desc"), ASC("asc");

	private String order;

	Order(String order) {
		this.order = order;
	}

	public String getOrder() {
		return order;
	}

}
