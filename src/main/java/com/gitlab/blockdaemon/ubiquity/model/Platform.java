package com.gitlab.blockdaemon.ubiquity.model;

/**
 * Non-exhaustive list of supported platforms
 *
 */
public enum Platform {

	ALGORAND("algorand"),
	BITCOIN("bitcoin"),
	BITCOIN_CASH("bitcoincash"),
	CELO("celo"),
	DOGECOIN("dogecoin"),
	ETHEREUM("ethereum"),
	LITECOIN("litecoin"),
	NEAR("near"),
	OASIS("oasis"),
	POLKADOT("polkadot"),
	RIPPLE("ripple"),
	SOLANA("solana"),
	STELLAR("stellar"),
	TEZOS("tezos"),
	OTHER();


	private String platform;

	Platform(String platform) {
		this.platform = platform;
	}

	Platform() {
	}

	public String getPlatform() {
		return this.platform;
	}

	public static Platform name(String platform) {
		final Platform otherPlatform = Platform.OTHER;
		otherPlatform.platform = platform;
		return otherPlatform;
	}
}
