package com.gitlab.blockdaemon.ubiquity.tx.btc;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

import com.gitlab.blockdaemon.ubiquity.tx.btc.exceptions.BitcoinException;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class BtcTransaction {
	public enum Type{
		TRANSACTION_TYPE_LEGACY, TRANSACTION_TYPE_BITCOIN_CASH, TRANSACTION_TYPE_SEGWIT;
	}

	private final int version;
	private final Input[] inputs;
	private final Output[] outputs;
	private final byte[][][] scriptWitnesses;
	private final int lockTime;

	public static BtcTransaction decodeTransaction(byte[] rawBytes) throws BitcoinException {
		try {
			return new BtcTransaction(rawBytes, true);
		} catch (final BitcoinException e) {
			if (e.errorCode == BitcoinException.ERR_WRONG_TYPE) {
				return new BtcTransaction(rawBytes, false);
			}
			throw e;
		}
	}

	/**
	 * Decodes transaction w/o BIP144 witness data
	 */
	public BtcTransaction(byte[] rawBytes) throws BitcoinException {
		this(rawBytes, false);
	}

	public BtcTransaction(byte[] rawBytes, boolean withWitness) throws BitcoinException {
		if (rawBytes == null) {
			throw new BitcoinException(BitcoinException.ERR_NO_INPUT, "empty input");
		}
		BitcoinInputStream bais = null;
		try {
			bais = new BitcoinInputStream(rawBytes);
			this.version = bais.readInt32();
			if (withWitness) {
				if (bais.readByte() != 0) {
					throw new BitcoinException(BitcoinException.ERR_WRONG_TYPE, "", this.version);
				}
				if (bais.readByte() == 0) {
					throw new BitcoinException(BitcoinException.ERR_WRONG_TYPE, "", this.version);
				}
			}
			final int inputsCount = (int) bais.readVarInt();
			this.inputs = new Input[inputsCount];
			for (int i = 0; i < inputsCount; i++) {
				final OutPoint outPoint = new OutPoint(BtcService.reverse(bais.readChars(32)), bais.readInt32());
				final byte[] script = bais.readChars((int) bais.readVarInt());
				final int sequence = bais.readInt32();
				this.inputs[i] = new Input(outPoint, new Script(script), sequence);
			}
			final int outputsCount = (int) bais.readVarInt();
			this.outputs = new Output[outputsCount];
			for (int i = 0; i < outputsCount; i++) {
				final long value = bais.readInt64();
				final long scriptSize = bais.readVarInt();
				if (scriptSize < 0 || scriptSize > 10_000_000) {
					throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Script size for output " + i +
							" is strange (" + scriptSize + " bytes).");
				}
				final byte[] script = bais.readChars((int) scriptSize);
				this.outputs[i] = new Output(value, new Script(script));
			}
			this.scriptWitnesses = new byte[withWitness ? inputsCount : 0][][];
			for (int i = 0; i < this.scriptWitnesses.length; i++) {
				final long stackItemsCount = bais.readVarInt();
				if (stackItemsCount < 0 || stackItemsCount > 10_000_000) {
					throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Stack count size " + i +
							" is strange (" + stackItemsCount + ").");
				}
				this.scriptWitnesses[i] = new byte[(int) stackItemsCount][];
				for (int j = 0; j < stackItemsCount; j++) {
					final long itemLength = bais.readVarInt();
					if (itemLength < 0 || itemLength > 10_000_000) {
						throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "Item length " + i + ' ' + j +
								" is strange (" + itemLength + " bytes).");
					}
					this.scriptWitnesses[i][j] = bais.readChars((int) itemLength);
				}
			}
			this.lockTime = bais.readInt32();
		} catch (final EOFException e) {
			throw new BitcoinException(BitcoinException.ERR_BAD_FORMAT, "TX incomplete");
		} catch (final IOException e) {
			throw new IllegalArgumentException("Unable to read TX");
		} catch (final Error e) {
			throw new IllegalArgumentException("Unable to read TX: " + e);
		} finally {
			if (bais != null) {
				try {
					bais.close();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}
	}


	public BtcTransaction(Input[] inputs, Output[] outputs, int lockTime) {
		this(1, inputs, outputs, lockTime, new byte[0][][]);
	}

	public BtcTransaction(int version, Input[] inputs, Output[] outputs, int lockTime) {
		this(version, inputs, outputs, lockTime, new byte[0][][]);
	}

	public BtcTransaction(int version, Input[] inputs, Output[] outputs, int lockTime, byte[][][] scriptWitnesses) {
		this.version = version;
		this.inputs = inputs;
		this.outputs = outputs;
		this.lockTime = lockTime;
		this.scriptWitnesses = scriptWitnesses;
	}

	boolean isCoinBase() {
		return this.inputs.length == 1 && this.inputs[0].outPoint.isNull();
	}

	public byte[] hash() {
		return BtcService.reverseInPlace(BtcService.doubleSha256(getBytes(false)));
	}

	public byte[] getBytes() {
		return getBytes(true);
	}

	@SuppressWarnings("unused")
	public String toHexEncodedString() {
		return BtcService.toHex(getBytes(true));
	}

	public byte[] getBytes(boolean withWitness) {
		if (withWitness && this.scriptWitnesses.length == 0) {
			withWitness = false;
		}
		final BitcoinOutputStream baos = new BitcoinOutputStream();
		try {
			baos.writeInt32(this.version);
			if (withWitness) {
				baos.write(0);
				baos.write(1);
			}
			baos.writeVarInt(this.inputs.length);
			for (final Input input : this.inputs) {
				baos.write(BtcService.reverse(input.outPoint.hash));
				baos.writeInt32(input.outPoint.index);
				final int scriptLen = input.scriptSig == null ? 0 : input.scriptSig.bytes.length;
				baos.writeVarInt(scriptLen);
				if (scriptLen > 0) {
					baos.write(input.scriptSig.bytes);
				}
				baos.writeInt32(input.sequence);
			}
			baos.writeVarInt(this.outputs.length);
			for (final Output output : this.outputs) {
				baos.writeInt64(output.value);
				final int scriptLen = output.scriptPubKey == null ? 0 : output.scriptPubKey.bytes.length;
				baos.writeVarInt(scriptLen);
				if (scriptLen > 0) {
					baos.write(output.scriptPubKey.bytes);
				}
			}
			if (withWitness) {
				for (final byte[][] witness : this.scriptWitnesses) {
					baos.writeVarInt(witness.length);
					for (final byte[] stackEntry : witness) {
						baos.writeVarInt(stackEntry.length);
						baos.write(stackEntry);
					}
				}
			}
			baos.writeInt32(this.lockTime);
		} catch (final IOException e) {
			e.printStackTrace();
		} finally {
			try {
				baos.close();
			} catch (final IOException e) {
				e.printStackTrace();
			}
		}
		return baos.toByteArray();

	}

	@Override
	public String toString() {
		return "{" +
				"\n\"inputs\":\n" + printAsJsonArray(this.inputs) +
				",\n\"outputs\":\n" + printAsJsonArray(this.outputs) +
				(this.scriptWitnesses.length == 0 ? "" : (",\n\"witnesses\":\n" + printWitnesses(this.scriptWitnesses))) +
				",\n\"lockTime\":\"" + this.lockTime + "\"}\n";
	}

	private String printWitnesses(byte[][][] scriptWitnesses) {
		final StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (int i = 0; i < scriptWitnesses.length; i++) {
			sb.append('[');
			if (scriptWitnesses[i] == null) {
				sb.append("[]");
			} else {
				for (int j = 0; j < scriptWitnesses[i].length; j++) {
					sb.append('[');
					sb.append(BtcService.toHex(scriptWitnesses[i][j]));
					sb.append(']');
					if (j != scriptWitnesses[i].length - 1) {
						sb.append(',');
					}
				}
			}
			sb.append(']');
			if (i != scriptWitnesses.length - 1) {
				sb.append(",\n");
			}
		}
		sb.append(']');
		return sb.toString();
	}

	private String printAsJsonArray(Object[] a) {
		if (a == null) {
			return "null";
		}
		if (a.length == 0) {
			return "[]";
		}
		final int iMax = a.length - 1;
		final StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (int i = 0; ; i++) {
			sb.append(a[i]);
			if (i == iMax) {
				return sb.append(']').toString();
			}
			sb.append(",\n");
		}
	}

	public int getWeightUnits() {
		final int legacySize = getBytes(false).length;
		final int witnessSize = getBytes(true).length - legacySize;
		return legacySize * 4 + witnessSize;
	}

	public int getVBytesSize() {
		return (int) Math.ceil(getWeightUnits() / 4f);
	}

	public static class Input {
		private final OutPoint outPoint;
		private final Script scriptSig;
		private final int sequence;

		public Input(OutPoint outPoint, Script scriptSig, int sequence) {
			this.outPoint = outPoint;
			this.scriptSig = scriptSig;
			this.sequence = sequence;
		}

		@Override
		public String toString() {
			return "{\n\"outPoint\":" + this.outPoint + ",\n\"script\":\"" + this.scriptSig + "\",\n\"sequence\":\"" + Integer.toHexString(this.sequence) + "\"\n}\n";
		}

		public OutPoint getOutPoint() {
			return this.outPoint;
		}

		public Script getScriptSig() {
			return this.scriptSig;
		}

		public int getSequence() {
			return this.sequence;
		}
	}

	public static class OutPoint {
		public final byte[] hash;//32-byte hash of the transaction from which we want to redeem an output
		public final int index;//Four-byte field denoting the output index we want to redeem from the transaction with the above hash (output number 2 = output index 1)

		public OutPoint(byte[] hash, int index) {
			this.hash = hash;
			this.index = index;
		}


		@Override
		public String toString() {
			return "{" + "\"hash\":\"" + BtcService.toHex(this.hash) + "\", \"index\":\"" + this.index + "\"}";
		}

		public boolean isNull() {
			return this.index == -1 && allZeroes(this.hash);
		}

		private static boolean allZeroes(byte[] hash) {
			for (final byte b : hash) {
				if (b != 0) {
					return false;
				}
			}
			return true;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			final OutPoint outPoint = (OutPoint) o;
			return this.index == outPoint.index && Arrays.equals(this.hash, outPoint.hash);
		}

		@Override
		public int hashCode() {
			int result = Arrays.hashCode(this.hash);
			result = 31 * result + this.index;
			return result;
		}
	}

	public static class Output {
		public final long value;
		public final Script scriptPubKey;

		public Output(long value,  Script scriptPubKey) {
			this.value = value;
			this.scriptPubKey = scriptPubKey;
		}


		@Override
		public String toString() {
			return "{\n\"value\":\"" + BtcService.formatValue(this.value) +
					"\",\"script\":\"" + this.scriptPubKey +
					"\",\"address\":" + getQuotedAddressInfo() + "\n}";
		}

		private String getQuotedAddressInfo() {
			if (this.scriptPubKey.isPay2PublicKeyHash()) {
				return "\"p2pkh prod " + getP2pkhAddress(false) + " or testnet " + getP2pkhAddress(true) + "\"";
			}
			if (this.scriptPubKey.isPayToScriptHash()) {
				return "\"p2sh prod " + getP2shAddress(false) + " or testnet " + getP2shAddress(true) + "\"";
			}
			final Script.WitnessProgram wp = this.scriptPubKey.getWitnessProgram();
			if (wp != null && wp.version == 0 && wp.program.length == 20) {
				try {
					return "\"p2wkh prod " + new Address(false, wp) + " or testnet " +
							new Address(true, wp) + "\"";
				} catch (final BitcoinException ignored) {
				}
			}
			return "\"unknown\"";
		}


		public String getP2pkhAddress(boolean testNet) {
			if (this.scriptPubKey.isPay2PublicKeyHash()) {
				final byte[] hash = new byte[20];
				System.arraycopy(this.scriptPubKey.bytes, 2, hash, 0, hash.length);
				return Address.ripemd160HashToAddress(testNet, hash);
			} else {
				return null;
			}
		}


		public String getP2shAddress(boolean testNet) {
			if (this.scriptPubKey.isPayToScriptHash()) {
				final byte[] hash = new byte[20];
				System.arraycopy(this.scriptPubKey.bytes, 2, hash, 0, hash.length);
				return Address.ripemd160HashToP2shAddress(testNet, hash);
			} else {
				return null;
			}
		}
	}

	public static class Checker {
		final int inputIndex;
		final long amount;
		final BtcTransaction spendTx;

		public Checker(int inputIndex, long amount, BtcTransaction spendTx) {
			this.inputIndex = inputIndex;
			this.amount = amount;
			this.spendTx = spendTx;
		}


		@Override
		public String toString() {
			return "Checker{" +
					"inputIndex=" + this.inputIndex +
					", amount=" + this.amount +
					", spendTx=" + this.spendTx +
					'}';
		}
	}


	public int getVersion() {
		return this.version;
	}

	public Input[] getInputs() {
		return this.inputs;
	}

	public Output[] getOutputs() {
		return this.outputs;
	}

	public byte[][][] getScriptWitnesses() {
		return this.scriptWitnesses;
	}

	public int getLockTime() {
		return this.lockTime;
	}

}
