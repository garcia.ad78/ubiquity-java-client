package com.gitlab.blockdaemon.ubiquity.ws;

import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Platform;
import com.gitlab.blockdaemon.ubiquity.tx.btc.TrulySecureRandom;

import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

@SuppressWarnings("rawtypes")
public class WebsocketClient extends WebSocketListener implements AutoCloseable {

	private static final Logger LOGGER = LoggerFactory.getLogger(TrulySecureRandom.class);

	private static final String SUBSCRIPTION_MESSAGE = "ubiquity.subscription";
	public final AtomicInteger requestIdBase = new AtomicInteger(1);

	// Pending bidirectional requests
	private final Map<Integer, CompletableFuture<Void>> pending = new ConcurrentHashMap<>();
	private final Map<Integer, Consumer<WebsocketResponse>> pendingRequests = new ConcurrentHashMap<>();

	// based on sub id
	private final Map<Integer, Subscription<?>> activeSubscriptions = new ConcurrentHashMap<>();
	private final List<Subscription<?>> addedSubscriptions = Collections.synchronizedList(new ArrayList<>());

	private final ObjectMapper mapper;
	private final Network network;
	private final Platform platform;

	private final AtomicBoolean starting = new AtomicBoolean(true);
	private final AtomicBoolean shouldBeOpen = new AtomicBoolean(true);


	private final ApiClient apiClient;
	private WebSocket webSocket;

	private final String wsBaseUrl;
	private long reconnectDelay = 5000L;

	public WebsocketClient(ApiClient apiClient, Platform platform, Network network) {
		this.wsBaseUrl = apiClient.getBasePath().replaceFirst("^https", "wss").replaceFirst("^http", "ws");
		this.mapper = apiClient.getJSON().getMapper();

		this.apiClient = apiClient;
		this.platform = platform;
		this.network = network;

		connect();
	}

	private void connect() {
		if(!this.shouldBeOpen.get()) {
			return;
		}

		if(this.webSocket != null) {
			this.webSocket.cancel();
		}

		final Map<String, String> headerParams = new HashMap<>();
		final Map<String, String> cookieParams = new HashMap<>();

		final String url = new StringBuilder().append(this.wsBaseUrl).append("/").append(this.platform.getPlatform())
				.append("/").append(this.network.getNetwork()).append("/websocket/").toString();

		final Request.Builder reqBuilder = new Request.Builder().url(url);
		this.apiClient.updateParamsForAuth(new String[] { "bearerAuth" }, Collections.emptyList(), headerParams, cookieParams);
		this.apiClient.processHeaderParams(headerParams, reqBuilder);
		this.apiClient.processCookieParams(cookieParams, reqBuilder);

		final Request request = reqBuilder.build();
		this.webSocket = this.apiClient.getHttpClient().newWebSocket(request, this);
		this.starting.set(false);
	}


	@Override
	public void onOpen(WebSocket webSocket, Response response) {
		// subscribe to anything requested
		LOGGER.debug("Connection open sending added subscriptions");
		for (final Subscription<?> subscription : this.addedSubscriptions) {
			subscription.setSubId(null);
			sendBidirectional(subscription.getSubscribeRequest(), websocketResponse -> this.processSubscriptionReply(subscription, websocketResponse));
		}
	}


	@Override
	public void onClosing(WebSocket webSocket, int code, String reason) {
		webSocket.close(1000, null);
		clearPendingRequests();
		// if the connection is closing but not because the client wants it closed but because the server is going away
		if (this.shouldBeOpen.get() && !this.starting.getAndSet(true) && code == 1001) {
			try {
				LOGGER.debug("Recieved going away message from server will attempt reconnect after delay");
				Thread.sleep(this.reconnectDelay);
				this.activeSubscriptions.clear();
				connect();
			} catch (final InterruptedException e) {
				LOGGER.error("Error in onClosing", e);
			}
		}
	}


	@Override
	public void onFailure(WebSocket webSocket, Throwable t, Response response) {
		LOGGER.error("Websocket connection failed");
		clearPendingRequests();
		if (this.shouldBeOpen.get() && !this.starting.getAndSet(true)) {
			try {
				LOGGER.debug("Websocket failed will attempt reconnect after delay");
				Thread.sleep(this.reconnectDelay);
				this.activeSubscriptions.clear();
				connect();
			} catch (final InterruptedException e) {
				LOGGER.error("Error in onFailure", e);
			}
		}
	}

	@Override
	public void onMessage(WebSocket webSocket, String text) {
		try {
			// map to type
			final WebsocketResponse websocketResponse = this.mapper.readValue(text, WebsocketResponse.class);

			if (SUBSCRIPTION_MESSAGE.equals(websocketResponse.getMethod())) {
				// Handle messages sent because of an active subscription
				LOGGER.debug("Recieved new message from subscription:: {}", websocketResponse);
				processMessage(webSocket, websocketResponse);
				return;
			}

			// Handle the response to a subscribe/unsubscribe
			LOGGER.debug("Recieved subscribe/unsubscribe response:: {}", websocketResponse);
			this.pendingRequests.remove(websocketResponse.getId()).accept(websocketResponse);

		} catch (final Exception e) {
			LOGGER.error("Error in onMessage", e);
		}
	}

	@SuppressWarnings("unchecked")
	private void processMessage(WebSocket webSocket, final WebsocketResponse websocketResponse) {
		for (final Notification notification : websocketResponse.getParams().getItems()) {
			if (this.activeSubscriptions.containsKey(notification.getSubID())) {
				final Subscription subscription = this.activeSubscriptions.get(notification.getSubID());
				subscription.getHandler().accept(this, notification);
			}
		}
	}

	private void clearPendingRequests() {
		this.pending.forEach((key, future) -> future.completeExceptionally(new SocketException("Connection Closing")));
		this.pending.clear();
		this.pendingRequests.clear();
	}

	private CompletableFuture<Void> sendBidirectional(WebsocketRequest request, Consumer<WebsocketResponse> callback) {
		final CompletableFuture<Void> completableFuture = new CompletableFuture<>();
		try {
			request.setId(getNextRequestId());
			final String requestJson = this.mapper.writeValueAsString(request);

			this.pending.put(request.getId(), completableFuture);
			this.pendingRequests.put(request.getId(), callback);

			this.webSocket.send(requestJson);
			return completableFuture;

		} catch (final JsonProcessingException e) {
			return CompletableFuture.failedFuture(e);
		}
	}


	private void processSubscriptionReply(final Subscription<?> subscription, final WebsocketResponse websocketResponse) {
		subscription.setSubId(websocketResponse.getResult().getSubID());

		this.activeSubscriptions.put(websocketResponse.getResult().getSubID(), subscription);

		if (this.pending.containsKey(websocketResponse.getId())) {
			this.pending.get(websocketResponse.getId()).complete(null);
			this.pending.remove(websocketResponse.getId());
		}
		this.pendingRequests.remove(websocketResponse.getId());

	}

	private void processUnsubscriptionReply(final Subscription<?> subscription, final WebsocketResponse websocketResponse) {
		if (websocketResponse.getResult().isRemoved()) {
			this.activeSubscriptions.remove(subscription.getSubId());
		}

		this.pending.get(websocketResponse.getId()).complete(null);
		this.pending.remove(websocketResponse.getId());
		this.pendingRequests.remove(websocketResponse.getId());
	}


	public Future<Void> subscribe(Subscription<?> subscription) {
		this.addedSubscriptions.add(subscription);
		return sendBidirectional(subscription.getSubscribeRequest(), websocketResponse -> this.processSubscriptionReply(subscription, websocketResponse));
	}


	public Future<Void> unsubscribe(Subscription<?> subscription) {
		this.addedSubscriptions.remove(subscription);
		return sendBidirectional(subscription.getUnsubscribeRequest(), websocketResponse -> this.processUnsubscriptionReply(subscription, websocketResponse));
	}


	public Future<Void> unsubscribe(Integer subId) {
		if(this.activeSubscriptions.containsKey(subId)) {
			return unsubscribe(this.activeSubscriptions.get(subId));
		}
		return CompletableFuture.completedFuture(null);
	}


	public Map<Integer, Subscription<?>> getActiveSubscriptions() {
		return this.activeSubscriptions;
	}

	/**
	 * Graceful shutdown - already-enqueued messages will continue to be transmitted however new messaged will be refused.
	 */
	@Override
	public void close() {
		this.shouldBeOpen.set(false);
		this.starting.set(false);
		this.activeSubscriptions.clear();
		clearPendingRequests();
		if (this.webSocket != null) {
			this.webSocket.close(1000, "requested");
		}
	}

	/**
	 * Non-graceful shutdown the websocket will close immediately.
	 */
	public void cancel() {
		this.shouldBeOpen.set(false);
		this.starting.set(false);
		this.activeSubscriptions.clear();
		clearPendingRequests();
		if (this.webSocket != null) {
			this.webSocket.cancel();
		}
	}

	private int getNextRequestId() {
		return this.requestIdBase.updateAndGet(n -> (n >= 1000000) ? 1 : n + 1);
	}

	public WebsocketClient reconnectDelay(long reconnectDelay) {
		this.reconnectDelay = reconnectDelay;
		return this;
	}

	public AtomicBoolean getOpen() {
		return this.shouldBeOpen;
	}
}
