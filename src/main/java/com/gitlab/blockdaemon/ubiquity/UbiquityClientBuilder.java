package com.gitlab.blockdaemon.ubiquity;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;

public class UbiquityClientBuilder {
	private String api = "https://ubiquity.api.blockdaemon.com/v1";
	private String auth;

	public UbiquityClientBuilder api(String api) {
		this.api = api;
		return this;
	}

	public UbiquityClientBuilder authBearerToken(String auth) {
		this.auth = auth;
		return this;
	}

	public UbiquityClient build() {
		ApiClient apiClient = Configuration.getDefaultApiClient();
		apiClient.getJSON().getMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		apiClient.setBearerToken(auth);
		apiClient.setBasePath(api);
		return new UbiquityClient(apiClient);
	}
}
